import { Outlet } from "react-router-dom";


const App = () => {
    <div className="container">
        TodoList
        <div className='mt-5'>
            <Outlet />
        </div>
    </div>
}

export default App;
