import { useGetAllTasksQuery } from "../../app/apiSlice.ts";


export const TaskList = () => {
    const [data, isLoading] = useGetAllTasksQuery()

    if (isLoading) return <div>Getting Tasks...</div>

    return (
        <div>
            <div>
                <div>
                    <table>
                        <thead>
                            <tr>
                                <th>Task</th>
                                <th>Due Date</th>
                                <th>Completed</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((task) => {
                                return (
                                    <tr key={task.id}r>
                                        <td>{task.task}</td>
                                        <td>{task.due_date}</td>
                                        <td>{task.completed}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
