import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'


export const todoListApi = createApi({
    reducerPath: 'todoListApi',
    baseQuery: fetchBaseQuery({baseUrl: proccess.env.REACT_APP_API_HOST}),
    tagTypes: ["Tasks", "Categories"],
    endpoints: (builder) => ({
        getAllTasks: builder.query<Array<object>, void>({
            query: () => ({
                url: '/api/tasks',
                credentials: 'include'
            }),
            providesTags: ["Tasks"]
        }),
        getOneTask: builder.query<object, string>({
            query: (task_id: string) => ({
                url: `/api/task/${task_id}`,
                method: "GET",
                credentials: 'include'
            }),
            providesTags: ["Tasks"]
        }),
        addTask: builder.mutation<object, object>({
            query: () => ({
                url: '/api/tasks/add',
                credentials: 'include'
            }),
            invalidatesTags: ["Tasks"]
        }),
        deleteTask: builder.mutation<boolean, string>({
            query: (task_id: string) => ({
                url: `/api/task/${task_id}`,
                method: "DELETE",
                credentials: 'include'
            }),
            invalidatesTags: ["Tasks"]
        }),
        updateTask: builder.mutation<object, string>({
            query: (task_id: string) => ({
                url: `/api/task/${task_id}`,
                method: "PUT",
                credentials: 'include'
            }),
            invalidatesTags: ["Tasks"]
        }),
        getAllCategories: builder.query<Array<object>, void>({
            query: () => ({
                url: '/api/categories',
                credentials: 'include'
            }),
            providesTags: ["Categories"]
        }),
        getOneCategory: builder.query<object, string>({
            query: (cat_id) => ({
                url: `/api/categories/${cat_id}`,
                method: "GET",
                credentials: 'include'
            }),
            providesTags: ["Categories"]
        }),
        addCategory: builder.mutation<object,object>({
            query: () => ({
                url: '/api/categories/',
                credentials: 'include'
            }),
            invalidatesTags: ["Categories"]
        }),
        deleteCategory: builder.mutation<boolean, string>({
            query: (cat_id) => ({
                url: `/api/categories/${cat_id}`,
                method: "DELETE",
                credentials: 'include'
            }),
            invalidatesTags: ["Categories"]
        }),
        updateCategories: builder.mutation<object, string>({
            query: (cat_id) => ({
                url: `/api/categories/${cat_id}`,
                method: "GET",
                credentials: 'include'
            }),
            invalidatesTags: ["Categories"]
        })
    })
})


export const {
    useGetAllTasksQuery,
    useGetOneTaskQuery,
    useAddTaskMutation,
    useDeleteTaskMutation,
    useUpdateTaskMutation,
    useGetAllCategoriesQuery,
    useGetOneCategoryQuery,
    useUpdateCategoriesMutation,
    useDeleteCategoryMutation
} = todoListApi
