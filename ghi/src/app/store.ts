import { configureStore } from '@reduxjs/toolkit'
import { todoListApi } from './apiSlice.ts'
import { setupListeners } from '@reduxjs/toolkit/dist/query'


export const store = configureStore({
    reducer: {
        [todoListApi.reducerPath]: todoListApi.reducer,
    },
})

setupListeners(store.dispatch)

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch
