from fastapi import FastAPI
from routers import tasks, categories

app = FastAPI()
app.include_router(tasks.router, tags=["Tasks"])
app.include_router(categories.router, tags=["Categories"])
