from fastapi import APIRouter, Depends
from models import TaskIn, TaskOut
from typing import List
from queries.tasks import TaskQueries


router = APIRouter()

@router.post("/api/tasks/add", response_model = TaskOut)
def add_task(task_in: TaskIn, queries: TaskQueries = Depends()):
    return queries.create(task_in)

@router.get("/api/tasks", response_model = List[TaskOut])
def get_tasks(qeuries: TaskQueries = Depends()):
    return qeuries.get_all()

@router.delete("/api/tasks/{task_id}", response_model = bool)
def delete_task(
    task_id: int,
    queries: TaskQueries = Depends(),
) -> bool:
    return queries.delete(task_id)

@router.get("/api/tasks/{task_id}", response_model= TaskOut)
def get_task(
    task_id: int,
    queries: TaskQueries = Depends(),
) -> TaskOut:
    return queries.get_one(task_id)

@router.put("/api/tasks/{task_id}", response_model = TaskOut)
def update_task(
    task_id: int,
    task: TaskIn,
    queries: TaskQueries = Depends()
) -> TaskOut:
    return queries.update(task_id, task)
