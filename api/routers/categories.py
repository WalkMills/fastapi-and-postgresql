from fastapi import APIRouter, Depends
from models import CategoryIn, CategoryOut
from queries.categories import CategoryQueries
from typing import List


router = APIRouter()


@router.post("/api/categories/add", response_model = CategoryOut)
def add_category(category_in: CategoryIn, queries: CategoryQueries = Depends()):
    return queries.create(category_in)

@router.get("/api/categories", response_model = List[CategoryOut])
def get_categories(queries: CategoryQueries = Depends()):
    return queries.get_all()

@router.delete("/api/categories/{cat_id}", response_model = bool)
def delete_category(cat_id: int, queries: CategoryQueries = Depends()) -> bool:
    return queries.delete(cat_id)

@router.get("/api/categories/{cat_id}", response_model = CategoryOut)
def get_category(cat_id: int, queries: CategoryQueries = Depends()) -> CategoryOut:
    return queries.get_one(cat_id)

@router.put("/api/categories/{cat_id}", response_model = CategoryOut)
def update_category(
    cat_id: int,
    category: CategoryIn,
    queries: CategoryQueries = Depends()
) -> CategoryOut:
    return queries.update(cat_id, category)
