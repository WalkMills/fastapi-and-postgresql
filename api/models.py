from pydantic import BaseModel


class TaskIn(BaseModel):
    task: str
    due_date: str | None
    completed: bool = False

class TaskOut(BaseModel):
    id: int
    task: str
    due_date: str | None
    completed: bool

class CategoryIn(BaseModel):
    name: str

class CategoryOut(BaseModel):
    id: int
    name: str
