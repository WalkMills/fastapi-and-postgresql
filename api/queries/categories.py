from models import CategoryOut, CategoryIn
from queries.pool import pool
from typing import List


class CategoryQueries:

    def category_in_to_out(self, id: int, category: CategoryOut):
        old_data = category.dict()
        return CategoryOut(id=id, **old_data)

    def record_to_category_out(self, record):
        return CategoryOut(
            id=record[0],
            name=record[1],
        )

    def create(self, category: CategoryIn) -> CategoryOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO categories
                        (name)
                    VALUES
                        (%s)
                    RETURNING id;
                    """,
                    [category.name]
                )
                id = result.fetchone()[0]
                return self.category_in_to_out(id, category)

    def get_all(self) -> List[CategoryOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name
                        FROM categories
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_category_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "error getting categories"}

    def update(self, cat_id: int, category: CategoryIn) -> CategoryOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE categories
                        SET name = %s
                        WHERE id = %s
                        """,
                        [category.name, cat_id]
                    )
                    return self.category_in_to_out(cat_id, category)

        except Exception as e:
            print(e)
            return {"message": "error updating category"}

    def get_one(self, cat_id: int) -> CategoryOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                          , name
                        FROM categories
                        WHERE id = %s
                        """,
                        [cat_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_category_out(record)

        except Exception as e:
            print(e)
            return {"message": "error getting category"}

    def delete(self, cat_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM categories
                        WHERE id = %s
                        """,
                        [cat_id]
                    )
                    return True

        except Exception as e:
            print(e)
            return False
