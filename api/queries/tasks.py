from models import TaskIn, TaskOut
from queries.pool import pool
from typing import List


class TaskQueries:

    def vacation_in_to_out(self, id: int, task: TaskOut):
        old_data = task.dict()
        return TaskOut(id=id, **old_data)

    def record_to_task_out(self, record):
        return TaskOut(
            id=record[0],
            task=record[1],
            due_date=record[2],
            completed=record[3]
        )

    def create(self, task: TaskIn) -> TaskOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO tasks
                        (task, due_date, completed)
                    VALUES
                        (%s, %s, %s)
                    RETURNING id;
                    """,
                    [
                     task.task,
                     task.due_date,
                     task.completed
                    ]
                )
                id = result.fetchone()[0]
                return self.vacation_in_to_out(id, task)

    def get_all(self) -> List[TaskOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, task, due_date, completed
                        FROM tasks
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_task_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "error getting tasks"}

    def update(self, task_id: int, task: TaskIn) -> TaskOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE tasks
                        SET task = %s
                          , due_date = %s
                          , completed = %s
                        WHERE id = %s
                        """,
                        [
                            task.task,
                            task.due_date,
                            task.completed,
                            task_id
                        ]
                    )
                    return self.vacation_in_to_out(task_id, task)

        except Exception as e:
            print(e)
            return {"message": "error updating task"}

    def get_one(self, task_id: int) -> TaskOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                          , task
                          , due_date
                          , completed
                        FROM tasks
                        WHERE id = %s
                        """,
                        [task_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_task_out(record)

        except Exception as e:
            print(e)
            return {"message": "error getting task"}

    def delete(self, task_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM tasks
                        WHERE id = %s
                        """,
                        [task_id]
                    )
                    return True

        except Exception as e:
            print(e)
            return False
