steps = [
    [
        ### Create the table
        """
        CREATE TABLE tasks (
            id SERIAL PRIMARY KEY NOT NULL,
            task VARCHAR(100) NOT NULL,
            due_date VARCHAR(100),
            completed BOOL
        );
        CREATE TABLE categories (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(100) NOT NULL
        );
        """,
        ### Drop the table
        """
        DROP TABLE tasks;
        DROP TABLE categories;
        """,
    ]
]
