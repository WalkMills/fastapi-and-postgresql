export {}

declare global {
    namespace FastAPI {
        interface ProcessEnv {
            [key: string]: string | undefined
            REACT_APP_API_HOST: string
        }
    }
}
